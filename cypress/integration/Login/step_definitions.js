import Method from '../libs/method';
import { When, Given, And, Then } from 'cypress-cucumber-preprocessor/steps';

let method = new Method();

//****************************************************************/
//****************************** When steps **********************/
//****************************************************************/

When("I push data : {string} in {string} field", (value, field_name) => {
    method.addValueInFieldByName(field_name, value);
    cy.log(value+" has been added successfully");
});

When("I click on {string}", (css_class) => {
    method.clickOnByClass(css_class);
});

//****************************************************************/
//****************************** Given steps **********************/
//****************************************************************/

Given("I'm connect on {string}", (url) => {
    cy.visit(url);
    cy.log("Connexion on " + url + "has been realized successfully");
});

//****************************************************************/
//****************************** Then steps **********************/
//****************************************************************/

Then("The login button is disabled", () =>{
    //method.checkFieldIsDisabled('element_type','button_ name'); 
    method.checkFieldIsDisabled('button','Log in'); 
});

Then("User is connected on app", () => {
    method.checkUserIsConnectedOnApp();
})