export default class Method {

    constructor(){

    }
    
    /**
     * Action description : This method get {selector} field in DOM and add value in field
     * Verification : After add, he verify if the value has been correcly added
     * @param {*} selector 
     * @param {*} value 
     */
    addValueInFieldByName(selector, value) {
        cy.get('[name='+selector+']')
                .type(value + '{moveToEnd}')
                .should('have.value', value);
        cy.log('Value : '+ value +' is correctly add in ' + selector + ' field');
    };

    /**
     * Action description : This method get {selector} field in DOM and search element's contains in field,  
     * Verification :  when element it's found he check if it's disbaled
     * @param {*} selector 
     * @param {*} containsValue 
     */
    checkFieldIsDisabled(selector, containsValue) {
        cy.get(selector)
            .contains(containsValue)
            .should('be.disabled');
    }

    /**
     * Action description : This method get {selector} field in DOM and search element. 
     * When element is found automate click on it
     * @param {*} selector 
     */
    clickOnByClass(selector) {
        cy.get('.'+ selector)
            .click();
        cy.log('Click on element with css class .'+ selector + 'works');
    }

    /**
     * Action description : This method get sign in button class and verifiy if it's visible.
     * If the button is visible, that mean login page is displayed correctly
     */
    checkLoginPageDisplayed() {
        cy.get('.signin-btn').should('be.visible');
        cy.log('Snapshift login page is displayed');
    }

    /**
     * Action description : This method verify if user is connected with URL
     * If URL is equal to https://app.snapshift.co/ that mean user is connected
     */
    checkUserIsConnectedOnApp() {
        cy.url().should('eq','https://app.snapshift.co/');
        cy.log('User is connected on Snapshift app');
    }

    /**
     * Action description : This method allows to click on href link.
     * @param {*} link 
     */
    clickOnHrefLink(link) {
        //cy.get('a[href="'+ link +'"]').click()
        cy.get('a[href*="/plannings/week"]').click()
    }

}