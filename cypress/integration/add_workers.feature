Feature: Add workers in Snapshift planning
Scenario: 
   Given Button "add workers" is displayed
   And Button "arranged workers" is displayed
   And Button "change counter" is displayed
   When Click on "add workers button"
   Then New workers form is displayed
   And Field "First name" is "empty"
   And Field "Last name" is "empty"
   And Field "Email" is "empty"
   And Field "Phone" is "empty"
   And Field "contract beginning" contain "13/03/2022"
   And Field "Hour beginnig contract" contain "08:00"
   And Field "contract type" is "empty"
   And Field "Time works" contain "5"
   And Field "Instituion" contain "Test"
   And The "saved employee button" is displayed

Scenario Outline: Scenario Outline
   Given Field "First name" is "empty"
   And Field "Last name" is "empty"
   And Field "Email" is "empty"
   And Field "Phone" is "empty"
   And Field "contract beginning" contain "13/03/2022"
   And Field "Hour beginnig contract" contain "08:00"
   And Field "contract type" is "empty"
   And Field "Time works" contain "5"
   And Field "Instituion" contain "Test"
   When I add <Last name> in "Last name" field
   And I add <First name> in "First name" field
   And I <Click button> on "beginnig button"
   And  I click on "saved employee button"
   Then Field "Last name" is <required 1>
   And Field "First name" is <required 2>
   And Field "contract beginning" is <required 3>
   And Workers is <created>
Examples:
    |Last name |First name |Click button    |required 1   |required 2   |required 3  |created     |
    |          |           |don't click     |required     |required     |not required|not created|
    |Marco     |           |don't click     |not required |required     |not required|not created|
    |          |Polo       |don't click     |required     |required     |not required|not created|
    |Marco     |Polo       |click           |not required |not required |required    |not created|
    |Marco     |Polo       |don't click     |not required |not required |not required|created    |
